<?php

/**
 * @file
 * Danger rose for avalanche advisories.
 */

/**
 * Rose class
 */
class Rose {
  var $_dest_path = NULL;
  var $_font_path = NULL;
  var $_watermark = NULL;

  var $_color_array = array("#999", "#0f0", "#ff3", "#f93", "#f00", "#000");
  var $_solid_array = array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
  var $_dot_array = array(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
  var $_draw_elevation = TRUE;
  var $_draw_legend = TRUE;
  var $_draw_minor_aspect = TRUE;
  var $_size_small = FALSE;

  var $_rose_width = 280;
  var $_rose_height = 173;

  var $_solid_array_target = "";
  var $_dot_array_target = "";

  // Easy function to set preferences to generate a smaller rose
  // with no elevation, legend, minor aspect, or watermark.
  function set_make_small_rose() {
    $this->_size_small = TRUE;
    $this->set_draw_elevation(FALSE);
    $this->set_draw_legend(FALSE);
    $this->set_draw_minor_aspect(FALSE);
    $this->set_watermark("");
    $this->set_width(173);
  }

  function set_solid_array_target($s) { $this->_solid_array_target = $s; }
  function get_solid_array_target() { return $this->_solid_array_target; }

  function set_dot_array_target($s) { $this->_dot_array_target = $s; }
  function get_dot_array_target() { return $this->_dot_array_target; }

  function set_draw_minor_aspect($s) { $this->_draw_minor_aspect = $s; }
  function get_draw_minor_aspect() { return $this->_draw_minor_aspect; }

  function set_width($w) { $this->_rose_width = $w; }
  function get_width() { return $this->_rose_width; }

  function set_height($h) { $this->_rose_height = $h; }
  function get_height() { return $this->_rose_height; }

  function get_make_small_rose() { return $this->_size_small; }

  function set_draw_elevation($s) { $this->_draw_elevation = $s; }
  function get_draw_elevation() { return $this->_draw_elevation; }

  function set_draw_legend($s) { $this->_draw_legend = $s; }
  function get_draw_legend() { return $this->_draw_legend; }

  function set_destination($s) { $this->_dest_path = $s; }
  function get_destination() { return $this->_dest_path; }

  function set_font_path($s) { $this->_font_path = $s; }
  function get_font_path() { return $this->_font_path; }

  function set_watermark($s) { $this->_watermark = $s; }
  function get_watermark() { return $this->_watermark; }

  function set_solid_array($a) { $this->_solid_array = $a; }
  function get_solid_array() { return $this->_solid_array; }

  function set_dot_array($a) { $this->_dot_array = $a; }
  function get_dot_array() { return $this->_dot_array; }

  function get_color_array() { return $this->_color_array; }

  function generate_gif($max_elev="", $mid_elev="", $min_elev="", $watermark=NULL) {
    require_once 'gdext.php';

    $base_font = $this->get_font_path() .'/slkscr.ttf';
    $label_font = $this->get_font_path() .'/vera.ttf';

    $w = $this->get_width();
    $h = $this->get_height();
    $img = imagecreatetruecolor($w, $h);

    if ($img) {
      imagesetthickness($img, 1);
      imagealphablending($img, TRUE);

      // base colors
      $white = imagecolorallocate($img, 255, 255, 255);
      $gray  = imagecolorallocate($img, 248, 248, 248);
      $lgray = imagecolorallocate($img, 153, 153, 153);
      $dgray = imagecolorallocate($img, 102, 102, 102);

      // danger ratings
      $black  = imagecolorallocate($img,   0,   0,   0);
      $red    = imagecolorallocate($img, 255,   0,   0);
      $orange = imagecolorallocate($img, 255, 153,  51);
      $yellow = imagecolorallocate($img, 255, 255,  51);
      $green  = imagecolorallocate($img,   0, 255,   0);

      imagefilledrectangle($img, 0, 0, $w, $h, $white);

      // base ellipse...
      imagefilledellipseaa($img, 85, 85, 124, 124, $lgray);

      // aspect letters
      imagettftext($img, 12, 0, 79, 18, $black, $label_font, "N");
      imagettftext($img, 12, 0, 80, 166, $black, $label_font, "S");
      imagettftext($img, 12, 0, 3, 92, $black, $label_font, "W");
      imagettftext($img, 12, 0, 152, 92, $black, $label_font, "E");

      if ($this->get_draw_minor_aspect()) {
        imagestring($img, 3,  23,  28, "NW", $lgray);
        imagestring($img, 3, 134,  28, "NE", $lgray);
        imagestring($img, 3,  23, 127, "SW", $lgray);
      }

      if ($this->get_draw_legend()) {
        // Danger legend
        imagefilledrectangle($img, 179,   9, 193, 20, $black);
        imagefilledrectangle($img, 180,  10, 192, 19, $black);
        imagefilledrectangle($img, 179,  27, 193, 39, $black);
        imagefilledrectangle($img, 180,  28, 192, 38, $red);
        imagefilledrectangle($img, 179,  45, 193, 57, $black);
        imagefilledrectangle($img, 180,  46, 192, 56, $orange);
        imagefilledrectangle($img, 179,  63, 193, 75, $black);
        imagefilledrectangle($img, 180,  64, 192, 74, $yellow);
        imagefilledrectangle($img, 179,  81, 193, 93, $black);
        imagefilledrectangle($img, 180,  82, 192, 92, $green);
        imagefilledellipseaa($img, 186, 108,   9,  9, $dgray);
        imagefilledellipseaa($img, 186, 108,   7,  7, $gray);

        // Danger legend text
        imagettftext($img, 6, 0, 198,  17, $dgray, $base_font, "Extreme");
        imagettftext($img, 6, 0, 198,  37, $dgray, $base_font, "High");
        imagettftext($img, 6, 0, 198,  55, $dgray, $base_font, "Considerable");
        imagettftext($img, 6, 0, 198,  72, $dgray, $base_font, "Moderate");
        imagettftext($img, 6, 0, 198,  91, $dgray, $base_font, "Low");
        imagettftext($img, 6, 0, 198, 108, $dgray, $base_font, "Pockets of");
        imagettftext($img, 6, 0, 198, 117, $dgray, $base_font, "higher danger");
      }

      $range = 3;
      $len = 125;
      $size = ($len / $range) - 10;
      $min_size = 50;

      $color = array();
      $color[0] = $lgray;
      $color[1] = $green;
      $color[2] = $yellow;
      $color[3] = $orange;
      $color[4] = $red;
      $color[5] = $black;

      $solid = $this->get_solid_array();
      $dot = $this->get_dot_array();

      $dot_coords = array(85, 31, 124, 48, 139, 85, 123, 124, 85, 140, 47, 123, 31, 85, 47, 47,
                          85, 47, 112, 59, 123, 85, 111, 112, 85, 123, 59, 111, 47, 85, 59, 59,
                          85, 65,  99, 72, 105, 85,  98,  99, 85, 105, 71,  99, 66, 85, 72, 72);

      $beg_deg = 248;
      $end_deg = 0;
      $max_deg = 360;
      $angle_deg = 45;

      $h = $len;
      $count = 0;
      $dot_counter = 0;

      // Draw the rose with the "solids" and "dots" values
      while ($h > $min_size) {
        for ($i = $count; $i < $count+8; $i++) {
          $end_deg = $beg_deg + $angle_deg;
          if ($end_deg > $max_deg) {
            $end_deg = $end_deg - $max_deg;
          }
          imagefilledarc($img, 85, 85, $h, $h, $beg_deg, $end_deg, $color[$solid[$i]], IMG_ARC_PIE|IMG_ARC_EDGED);

          $a = $dot_coords[$dot_counter];
          $dot_counter++;
          $b = $dot_coords[$dot_counter];

          $d = intval($dot[$i]);

          if ($d == 0) {
            imagefilledellipseaa($img, intval($a), intval($b), 9, 9, $lgray);
          }
          else if ($d == 1) {
            imagefilledellipseaa($img, intval($a), intval($b), 9, 9, $green);
          }
          else if ($d == 2) {
            imagefilledellipseaa($img, intval($a), intval($b), 9, 9, $yellow);
          }
          else if ($d == 3) {
            imagefilledellipseaa($img, intval($a), intval($b), 9, 9, $orange);
          }
          else if ($d == 4) {
            imagefilledellipseaa($img, intval($a), intval($b), 9, 9, $red);
          }
          else {
            imagefilledellipseaa($img, intval($a), intval($b), 9, 9, $black);
          }

          $beg_deg = $end_deg;
          $dot_counter++;
        }

        $count += 8;
        $h = $h - $size;
      }

      // Generate the elevation "rings" overlay
      //imageantialias($img, TRUE);
      $h = $len;
      while ($h > $min_size) {
        imagefilledarc($img, 85, 85, $h, $h, 248, 293, $dgray, IMG_ARC_PIE|IMG_ARC_NOFILL|IMG_ARC_EDGED);
        imagefilledarc($img, 85, 85, $h, $h, 293, 338, $dgray, IMG_ARC_PIE|IMG_ARC_NOFILL|IMG_ARC_EDGED);
        imagefilledarc($img, 85, 85, $h, $h, 338,  23, $dgray, IMG_ARC_PIE|IMG_ARC_NOFILL|IMG_ARC_EDGED);
        imagefilledarc($img, 85, 85, $h, $h,  23,  68, $dgray, IMG_ARC_PIE|IMG_ARC_NOFILL|IMG_ARC_EDGED);
        imagefilledarc($img, 85, 85, $h, $h,  68, 113, $dgray, IMG_ARC_PIE|IMG_ARC_NOFILL|IMG_ARC_EDGED);
        imagefilledarc($img, 85, 85, $h, $h, 113, 158, $dgray, IMG_ARC_PIE|IMG_ARC_NOFILL|IMG_ARC_EDGED);
        imagefilledarc($img, 85, 85, $h, $h, 158, 203, $dgray, IMG_ARC_PIE|IMG_ARC_NOFILL|IMG_ARC_EDGED);
        imagefilledarc($img, 85, 85, $h, $h, 203, 248, $dgray, IMG_ARC_PIE|IMG_ARC_NOFILL|IMG_ARC_EDGED);

        $h = $h - $size;
      }

      // Draw the elevation line(s) and label(s)... if they've been set.
      if ($this->get_draw_elevation()) {
        //imageantialias($img, TRUE);
        if (strlen($max_elev) > 0) {
          imageline($img, 97, 113, 141, 128, $dgray);
          imagettftext($img, 6, 0, 145, 133, $dgray, $base_font, $max_elev);
        }
        if (strlen($mid_elev) > 0) {
          imageline($img, 102, 128, 131, 138, $dgray);
          imagettftext($img, 6, 0, 135, 143, $dgray, $base_font, $mid_elev);
        }
        if (strlen($min_elev) > 0) {
          imageline($img, 107, 142, 121, 147, $dgray);
          imagettftext($img, 6, 0, 125, 153, $dgray, $base_font, $min_elev);
        }
      }

      // Setup the bounding box for the watermark and apply it
      // against the larger summary roses.
      $wm = "";
      if ($watermark != NULL) {
        $wm = $watermark;
      }

      $bbox = imagettfbbox(6, 0, $base_font, $wm);
      $bbox_h = abs($bbox[5]-$bbox[3]);
      $bbox_w = abs($bbox[2]-$bbox[0]);
      $bbox_x = $w - $bbox_w;

      $d = $this->get_destination();
      if ($d == NULL) {
        header("Content-type: image/gif");
      }
      if ($this->get_make_small_rose()) {
        $ow = $this->get_width();
        $oh = $this->get_height();
        $nw = 100;
        $nh = 100;
        $nimg = imagecreatetruecolor($nw, $nh);

        imagecopyresampled($nimg, $img, 0, 0, 0, 0, $nw, $nh, $ow, $oh);
        imagegif($nimg, $d);
        imagedestroy($nimg);
      }
      else {
        imagettftext($img, 6, 0, $bbox_x - 5, 168, $lgray, $base_font, $wm);
        imagegif($img, $d);
      }
      imagedestroy($img);
    }
  }

  function generate_svg() {
    $solids = $this->get_solid_array();
    $dots = $this->get_dot_array();
    $colors = $this->get_color_array();
    $color_string = join('","', $colors);
    $solids_string = join(',', $solids);
    $dots_string = join(',', $dots);
    $sat = $this->get_solid_array_target();
    $dat = $this->get_dot_array_target();

    $a = array();
    $b = array();
    foreach ($solids as $v) {
      $a[] = $colors[$v];
    }
    foreach ($dots as $v) {
      $b[] = $colors[$v];
    }

    $svg = <<< END
<?xml version="1.0" standalone="no"?>
<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">
<svg width="475" height="285" version="1.1" xmlns="http://www.w3.org/2000/svg" onmouseout="copy_array()">
  <script type="text/javascript">
  <![CDATA[
    var idx = 0;
    var colors = new Array("$color_string");
    var rose = new Array($solids_string);
    var rose_dot = new Array($dots_string);
    var solid_array_target = "$sat";
    var dot_array_target = "$dat";

    // Sets the active color (danger level) to paint with.
    function set_color(i) {
      idx = i;
      e = document.getElementById("active_color");
      e.setAttributeNS(null, "fill", colors[idx]);
    }

    // Sets the color (danger level) of the element.
    // If the alt key is held down, it paints by aspect.
    function fill(ev, id) {
      var e = "";
      var ids = [];
      ids[0] = id;

      if (ev.altKey) {
        var max = 7;
        if (id < 8) { max = 7; }
        else if (id < 16) { max = 15; }
        else { max = 23; }
        for (i = 0; i < 8; i++) {
          ids[i] = max;
          max--;
        }
      }

      var len = ids.length;

      for (i = 0; i < len; i++) {
        id = ids[i];
        e = document.getElementById(id);
        if (e.getAttributeNS(null, "fill") == colors[idx]) {
          e = document.getElementById("dot_"+ id);
          if (e.getAttributeNS(null, "fill") == colors[idx]) {
            e.setAttributeNS(null, "fill", colors[idx+1]);
            rose[id] = idx;
            rose_dot[id] = idx+1;
          } else {
            e.setAttributeNS(null, "fill", colors[idx]);
            rose[id] = idx;
            rose_dot[id] = idx;
          }
        } else {
          e.setAttributeNS(null, "fill", colors[idx]);
          rose[id] = idx;
          rose_dot[id] = idx;
          e = document.getElementById("dot_"+ id);
          e.setAttributeNS(null, "fill", colors[idx]);
        }
      }
    }

    // Copies the values of the solids/dots into form for processing on submit.
    function copy_array() {
      //FIXME: check for these so alerts aren't thrown
      window.parent.document.forms['node-form'].elements[solid_array_target].value = rose.join(",");
      window.parent.document.forms['node-form'].elements[dot_array_target].value = rose_dot.join(",");
    }

    // Fills by aspect.  duh.
    function fill_aspect(a) {
      var len = a.length;
      for (j = 0; j < len; j++) { fill("", a[j]); }
    }
  ]]>
  </script>

  <rect x="0" y="0" width="475" height="285" style="fill:#eee;"/>

  <g transform="translate(7,5)">
    <text x="255" y="10" style="font-size:7pt; font-weight:bold; fill:#000;">Directions:</text>
    <text x="255" y="22" style="font-size:7pt; font-weight:normal; fill:#666;">Click a rating and a region in the rose.</text>
    <text x="255" y="32" style="font-size:7pt; font-weight:normal; fill:#666;">Click again to add the &quot;pepperoni.&quot;</text>

    <text x="130" y="15" style="font-size:12pt; font-weight:bold; fill:#666;" onmousedown="fill_aspect(new Array(0,8,16))">N</text>
    <text x="220" y="50" style="font-size:10pt; font-weight:bold; fill:#999;" onmousedown="fill_aspect(new Array(1,9,17))">NE</text>
    <text x="255" y="140" style="font-size:12pt; font-weight:bold; fill:#666;" onmousedown="fill_aspect(new Array(2,10,18))">E</text>
    <text x="220" y="230" style="font-size:10pt; font-weight:bold; fill:#999;" onmousedown="fill_aspect(new Array(3,11,19))">SE</text>
    <text x="130" y="268" style="font-size:12pt; font-weight:bold; fill:#666;" onmousedown="fill_aspect(new Array(4,12,20))">S</text>
    <text x="30" y="230" style="font-size:10pt; font-weight:bold; fill:#999;" onmousedown="fill_aspect(new Array(5,13,21))">SW</text>
    <text x="0" y="140" style="font-size:12pt; font-weight:bold; fill:#666;" onmousedown="fill_aspect(new Array(6,14,22))">W</text>
    <text x="30" y="50" style="font-size:10pt; font-weight:bold; fill:#999;" onmousedown="fill_aspect(new Array(7,15,23))">NW</text>

    <rect x="295" y="50" width="25" height="25" style="fill:#999; stroke:#666;" onmousedown="set_color(0)"/>
    <text x="325" y="67" style="font-size:8pt; font-weight:bold; fill:#666;" onmousedown="set_color(0)">None</text>

    <rect x="295" y="80" width="25" height="25" style="fill:#000; stroke:#666;" onmousedown="set_color(5)"/>
    <text x="325" y="97" style="font-size:8pt; font-weight:bold; fill:#666;" onmousedown="set_color(5)">Extreme</text>

    <rect x="295" y="110" width="25" height="25" style="fill:#f00; stroke:#666;" onmousedown="set_color(4)"/>
    <text x="325" y="127" style="font-size:8pt; font-weight:bold; fill:#666;" onmousedown="set_color(4)">High</text>

    <rect x="295" y="140" width="25" height="25" style="fill:#f93; stroke:#666;" onmousedown="set_color(3)"/>
    <text x="325" y="157" style="font-size:8pt; font-weight:bold; fill:#666;" onmousedown="set_color(3)">Considerable</text>

    <rect x="295" y="170" width="25" height="25" style="fill:#ff3; stroke:#666;" onmousedown="set_color(2)"/>
    <text x="325" y="187" style="font-size:8pt; font-weight:bold; fill:#666;" onmousedown="set_color(2)">Moderate</text>

    <rect x="295" y="200" width="25" height="25" style="fill:#0f0; stroke:#666;" onmousedown="set_color(1)"/>
    <text x="325" y="217" style="font-size:8pt; font-weight:bold; fill:#666;" onmousedown="set_color(1)">Low</text>

    <text x="255" y="250" style="font-size:7pt; font-weight:bold; fill:#000;">Tips:</text>
    <text x="255" y="262" style="font-size:7pt; font-weight:normal; fill:#666;">Click aspect label to select all elevations.</text>
    <text x="255" y="272" style="font-size:7pt; font-weight:normal; fill:#666;">Alt/Opt-click to select all aspects.</text>
  </g>

  <g transform="translate(7,5) rotate(23 135,135)">

    <circle cx="135" cy="135" r="112" style="fill:#999; stroke:#666; stroke-width:2"/>
    <circle cx="135" cy="135" r="85" style="fill:#999; stroke:#666; stroke-width:2"/>
    <circle cx="135" cy="135" r="55" style="fill:#999; stroke:#666; stroke-width:2"/>

    <!-- start at top, clockwise, outer ring -->
    <path d="M135,135 L55,55 A112,112 0 0 1 135,23 Z" fill="$a[0]" id="0" style="stroke:#666; stroke-width:2" onmousedown="fill(evt,0)"/>
    <path d="M135,135 L135,23 A112,112 0 0 1 214,55 Z" fill="$a[1]" id="1" style="stroke:#666; stroke-width:2" onmousedown="fill(evt,1)"/>
    <path d="M135,135 L214,55 A112,112 0 0 1 246,135 Z" fill="$a[2]" id="2" style="stroke:#666; stroke-width:2" onmousedown="fill(evt,2)"/>
    <path d="M135,135 L246,135 A112,112 0 0 1 214,214 Z" fill="$a[3]" id="3" style="stroke:#666; stroke-width:2" onmousedown="fill(evt,3)"/>
    <path d="M135,135 L214,214 A112,112 0 0 1 134,246 Z" fill="$a[4]" id="4" style="stroke:#666; stroke-width:2" onmousedown="fill(evt,4)"/>
    <path d="M135,135 L134,246 A112,112 0 0 1 55,214 Z" fill="$a[5]" id="5" style="stroke:#666; stroke-width:2" onmousedown="fill(evt,5)"/>
    <path d="M135,135 L55,214 A112,112 0 0 1 23,134 Z" fill="$a[6]" id="6" style="stroke:#666; stroke-width:2" onmousedown="fill(evt,6)"/>
    <path d="M135,135 L23,134 A112,112 0 0 1 55,55 Z" fill="$a[7]" id="7" style="stroke:#666; stroke-width:2" onmousedown="fill(evt,7)"/>

    <!-- start at top, clockwise, middle ring -->
    <path d="M135,135 L74,74 A85,85 0 0 1 135,50 Z" fill="$a[8]" id="8" style="stroke:#666; stroke-width:2" onmousedown="fill(evt,8)"/>
    <path d="M135,135 L135,50 A85,85 0 0 1 195,74 Z" fill="$a[9]" id="9" style="stroke:#666; stroke-width:2" onmousedown="fill(evt,9)"/>
    <path d="M135,135 L195,74 A85,85 0 0 1 219,135 Z" fill="$a[10]" id="10" style="stroke:#666; stroke-width:2" onmousedown="fill(evt,10)"/>
    <path d="M135,135 L219,135 A85,85 0 0 1 195,195 Z" fill="$a[11]" id="11" style="stroke:#666; stroke-width:2" onmousedown="fill(evt,11)"/>
    <path d="M135,135 L195,195 A85,85 0 0 1 134,219 Z" fill="$a[12]" id="12" style="stroke:#666; stroke-width:2" onmousedown="fill(evt,12)"/>
    <path d="M135,135 L134,219 A85,85 0 0 1 74,195 Z" fill="$a[13]" id="13" style="stroke:#666; stroke-width:2" onmousedown="fill(evt,13)"/>
    <path d="M135,135 L74,195 A85,85 0 0 1 50,134 Z" fill="$a[14]" id="14" style="stroke:#666; stroke-width:2" onmousedown="fill(evt,14)"/>
    <path d="M135,135 L50,134 A85,85 0 0 1 74,74 Z" fill="$a[15]" id="15" style="stroke:#666; stroke-width:2" onmousedown="fill(evt,15)"/>

    <!-- start at top, clockwise, inner ring -->
    <path d="M135,135 L96,96 A55,55 0 0 1 135,80 Z" fill="$a[16]" id="16" style="stroke:#666; stroke-width:2" onmousedown="fill(evt,16)"/>
    <path d="M135,135 L135,80 A55,55 0 0 1 173,96 Z" fill="$a[17]" id="17" style="stroke:#666; stroke-width:2" onmousedown="fill(evt,17)"/>
    <path d="M135,135 L173,96 A55,55 0 0 1 189,135 Z" fill="$a[18]" id="18" style="stroke:#666; stroke-width:2" onmousedown="fill(evt,18)"/>
    <path d="M135,135 L189,135 A55,55 0 0 1 173,173 Z" fill="$a[19]" id="19" style="stroke:#666; stroke-width:2" onmousedown="fill(evt,19)"/>
    <path d="M135,135 L173,173 A55,55 0 0 1 134,189 Z" fill="$a[20]" id="20" style="stroke:#666; stroke-width:2" onmousedown="fill(evt,20)"/>
    <path d="M135,135 L134,189 A55,55 0 0 1 96,173 Z" fill="$a[21]" id="21" style="stroke:#666; stroke-width:2" onmousedown="fill(evt,21)"/>
    <path d="M135,135 L96,173 A55,55 0 0 1 80,134 Z" fill="$a[22]" id="22" style="stroke:#666; stroke-width:2" onmousedown="fill(evt,22)"/>
    <path d="M135,135 L80,134 A55,55 0 0 1 96,96 Z" fill="$a[23]" id="23" style="stroke:#666; stroke-width:2" onmousedown="fill(evt,23)"/>
  </g>

  <g transform="translate(-3,0)">
    <!-- (dots) start at n, clockwise, outer ring -->
    <circle cx="145" cy="40" r="8" style="stroke:#666; stroke-width:0" fill="$b[0]" id="dot_0" onmousedown="fill(evt,0)"/>
    <circle cx="215" cy="69" r="8" style="stroke:#666; stroke-width:0" fill="$b[1]" id="dot_1" onmousedown="fill(evt,1)"/>
    <circle cx="244" cy="140" r="8" style="stroke:#666; stroke-width:0" fill="$b[2]" id="dot_2" onmousedown="fill(evt,2)"/>
    <circle cx="215" cy="210" r="8" style="stroke:#666; stroke-width:0" fill="$b[3]" id="dot_3" onmousedown="fill(evt,3)"/>
    <circle cx="145" cy="239" r="8" style="stroke:#666; stroke-width:0" fill="$b[4]" id="dot_4" onmousedown="fill(evt,4)"/>
    <circle cx="73"  cy="208" r="8" style="stroke:#666; stroke-width:0" fill="$b[5]" id="dot_5" onmousedown="fill(evt,5)"/>
    <circle cx="45"  cy="140" r="8" style="stroke:#666; stroke-width:0" fill="$b[6]" id="dot_6" onmousedown="fill(evt,6)"/>
    <circle cx="72"  cy="71" r="8" style="stroke:#666; stroke-width:0" fill="$b[7]" id="dot_7" onmousedown="fill(evt,7)"/>

    <!-- (dots) start at n, clockwise, middle ring -->
    <circle cx="145" cy="70" r="8" style="stroke:#666; stroke-width:0" fill="$b[8]" id="dot_8" onmousedown="fill(evt,8)"/>
    <circle cx="193" cy="90" r="8" style="stroke:#666; stroke-width:0" fill="$b[9]" id="dot_9" onmousedown="fill(evt,9)"/>
    <circle cx="214" cy="140" r="8" style="stroke:#666; stroke-width:0" fill="$b[10]" id="dot_10" onmousedown="fill(evt,10)"/>
    <circle cx="193" cy="188" r="8" style="stroke:#666; stroke-width:0" fill="$b[11]" id="dot_11" onmousedown="fill(evt,11)"/>
    <circle cx="145" cy="209" r="8" style="stroke:#666; stroke-width:0" fill="$b[12]" id="dot_12" onmousedown="fill(evt,12)"/>
    <circle cx="95"  cy="188" r="8" style="stroke:#666; stroke-width:0" fill="$b[13]" id="dot_13" onmousedown="fill(evt,13)"/>
    <circle cx="75"  cy="140" r="8" style="stroke:#666; stroke-width:0" fill="$b[14]" id="dot_14" onmousedown="fill(evt,14)"/>
    <circle cx="95"  cy="91" r="8" style="stroke:#666; stroke-width:0" fill="$b[15]" id="dot_15" onmousedown="fill(evt,15)"/>

    <!-- (dots) start at n, clockwise, inner ring -->
    <circle cx="145" cy="102" r="8" style="stroke:#666; stroke-width:0" fill="$b[16]" id="dot_16" onmousedown="fill(evt,16)"/>
    <circle cx="171" cy="114" r="8" style="stroke:#666; stroke-width:0" fill="$b[17]" id="dot_17" onmousedown="fill(evt,17)"/>
    <circle cx="181" cy="140" r="8" style="stroke:#666; stroke-width:0" fill="$b[18]" id="dot_18" onmousedown="fill(evt,18)"/>
    <circle cx="171" cy="166" r="8" style="stroke:#666; stroke-width:0" fill="$b[19]" id="dot_19" onmousedown="fill(evt,19)"/>
    <circle cx="145" cy="177" r="8" style="stroke:#666; stroke-width:0" fill="$b[20]" id="dot_20" onmousedown="fill(evt,20)"/>
    <circle cx="118" cy="166" r="8" style="stroke:#666; stroke-width:0" fill="$b[21]" id="dot_21" onmousedown="fill(evt,21)"/>
    <circle cx="109" cy="140" r="8" style="stroke:#666; stroke-width:0" fill="$b[22]" id="dot_22" onmousedown="fill(evt,22)"/>
    <circle cx="118" cy="113" r="8" style="stroke:#666; stroke-width:0" fill="$b[23]" id="dot_23" onmousedown="fill(evt,23)"/>
  </g>

  <circle cx="15" cy="15" r="6" style="stroke:#999; stroke-width:1" fill="#999" id="active_color"/>
  <text x="26" y="18" style="font-size:7pt; font-weight:normal; fill:#666;">Active color</text>
</svg>
END;

    return $svg;
  }
}
