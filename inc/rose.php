<?php

/**
 * @file
 * Loader for danger rose, since SVG is *more* compatible if loaded
 * in an iframe instead of inline or <object>.
 */

require('rose.inc');
$rose = new Rose;

// FIXME: perform sanity check of incoming string
if (count($_GET) > 0) {
  if (count($_GET['s']) > 0) {
    $s = split(',', $_GET['s']);
    $rose->set_solid_array(split(',', $_GET['s']));
    $rose->set_dot_array(split(',', $_GET['d']));
  }
  $rose->set_solid_array_target($_GET['a']);
  $rose->set_dot_array_target($_GET['b']);
}

header("Content-type: image/svg+xml");
print $rose->generate_svg();

?>
