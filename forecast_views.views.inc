<?php

// Just a proof of concept... doesn't do anything very interesting.
// Some use-cases would be nice since I'm not sure how a forecaster
// or snow science geek might use this information.

function forecast_views_data() {
  $data = array();

  $data['forecast_advisory'] = array(
      'table' => array(
          'base' => array(
              'field' => 'rid',
              'title' => t('Advisory'),
          ),
          'group' => t('Advisory'),
          'title' => 'form_values',
          'join' => array(
              'node' => array(
                  'left_field' => 'nid',
                  'field' => 'nid',        
                ),
            ),
      ),
      'form_values' => array(
          'title' => t('Form Values'),
          'field' => array(
              'handler' => 'views_handler_field_node',
              'click sortable' => TRUE,
          ),
      ),
  );

  return $data;
}
