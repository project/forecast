<?php

/**
 * Implementation of hook_settings()
 *
 * Enable/disable the condtions required to send an alert.
 */ 
function forecast_alerts_settings() {
  $form['forecast_alerts_types'] = array(
      '#type' => 'checkboxes',
      '#title' => t('Alert conditions'),
      '#default_value' => variable_get(
          'forecast_alerts_types', array(
              'alert_warning',
              'alert_watch',
              'extended_forecast_rating_0',
          )   
      ),  
      '#options' => array(
          'alert_warning' => t('When an avalanche warning is issued.'),
          'alert_watch' => t('When an avalanche watch is issued.'),
          'extended_forecast_rating_0' => t('When the danger rating changes.')
      ),  
      '#description' => t('When the checked conditions are met, a mobile alert will be sent.')
  );  

  return system_settings_form($form);
}


/**
 * Implementation of hook_settings().
 *
 * Settings for static text alerts.  Appropriate region information will be
 * added to the alert.
 */
function forecast_alerts_text_settings() {
  $form = array();

  $form['forecast_alerts_footer_text'] = array(
      '#type' => 'textfield',
      '#title' => t('Alerts footer text'),
      '#description' => t('This text will be appended to the standard alert text.'),
      '#size' => 50,
      '#maxlength' => 100,
      '#default_value' => variable_get('forecast_alerts_footer_text', 'Check utahavalanchecenter.org for more details or call 1-888-999-4019.'),
  );

  $form['forecast_alerts_center_text'] = array(
      '#type' => 'textfield',
      '#title' => t('Avalanche center name'),
      '#description' => t('The official name of your avalanche center.'),
      '#size' => 50,
      '#maxlength' => 100,
      '#default_value' => variable_get('forecast_alerts_center_text', 'The Utah Avalanche Center'),
  );

  return system_settings_form($form);
}
