<?php

/**
 * Implementation of hook_settings().
 *
 * Settings to control which components are enabled in the advisory.
 */
function forecast_advisory_settings() {
  $form['forecast_components'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Components'),
    '#default_value' => variable_get(
        'forecast_components', array(
            'Activity',
            'Alert',
            'Announcement',
            'Condition',
            'Extended forecast',
            'Static text',
            'Summary',
            'Threat',
            'Weather'
        )    
    ),   
    '#options' => array(
        'Activity' => t('Activity: Recent'),
        'Alert' => t('Alerts: Warning, Watch'),
        'Announcement' => t('Announcements: General, Special'),
        'Condition' => t('Conditions: Current'),
        'Extended forecast' => t('Extended forecast: Today, tomorrow, etc.'),
        'Static text' => t('Static text: Disclaimer, Provider'),
        'Summary' => t('Summary: Bottom line'),
        'Threat' => t('Threats: Danger rose, Probability, Problem, Size, Trend'),
        'Weather' => t('Weather: Mountain')
    ),   
    '#description' => t('Select the component(s) that will be used in an advisory.')
  );

  return system_settings_form($form);
}


/**
 * Implementation of hook_settings().
 *
 * Elevation settings for the danger rose.  If these are set, they will 
 * be overlayed on the generated danger rose (i.e. 6,000ft., above treeline).
 */
function forecast_elevation_settings($form_state) {
  $result = db_query(_sql_select_elevation_settings());

  $rows[] = array();
  while ($row = db_fetch_array($result, MYSQL_ASSOC)) {
    $rows[$row['region']] = array($row['base'], $row['mid'], $row['upper']);
  }

  $regions = array();
  $tree = forecast_get_regions();
  foreach ($tree as $region) {
    $regions[] = $region->name;
  }

  foreach ($regions as $region) {
    $r = str_replace(" ", "_", $region);
    $form['region'][$r][$r .'-base'] = array(
        '#type' => 'textfield',
        '#size' => 15,
        '#default_value' => $rows[$region][0]
    );
    $form['region'][$r][$r .'-mid'] = array(
        '#type' => 'textfield',
        '#size' => 15,
        '#default_value' => $rows[$region][1]
    );
    $form['region'][$r][$r .'-upper'] = array(
        '#type' => 'textfield',
        '#size' => 15,
        '#default_value' => $rows[$region][2]
    );
  }

  $form['submit'] = array('#type' => 'submit', '#value' => t('Save configuration'));
  $form['#submit'] = array('forecast_elevation_settings_submit');

  return $form;
}

function theme_forecast_elevation_settings($form) {
  $header = array(t('Region'), t('Base'), t('Mid'), t('Upper'));

  $rows = array();
  foreach (element_children($form['region']) as $key) {
    $region = str_replace("_", " ", $key);
    $row = array($region,
        drupal_render($form['region'][$key][$key .'-base']),
        drupal_render($form['region'][$key][$key .'-mid']),
        drupal_render($form['region'][$key][$key .'-upper'])
    );
    $rows[] = $row;
  }

  $output = theme('table', $header, $rows);
  $output .= drupal_render($form);

  return $output;
}


/**
 * Submit handler
 *
 * Saves elevation settings.
 */
function forecast_elevation_settings_submit($form, &$form_state) {
  $regions = array();
  $tree = forecast_get_regions();
  $fsv = $form_state['values'];

  foreach ($tree as $region) {
    $r = str_replace(" ", "_", $region->name);
    db_query(_sql_delete_elevation_settings(), $region->name);
    db_query(_sql_insert_elevation_settings(),
         $region->name, $fsv[$r .'-base'], $fsv[$r .'-mid'], $fsv[$r .'-upper']);
  }
}


/**
 * Implementation of hook_settings().
 *
 * Settings for static text (i.e. disclaimer) and watermark (for roses).
 */
function forecast_advisory_text_settings() {
  $result = db_query(_sql_select_static_text());
  $text = array();
  while ($row = db_fetch_array($result, MYSQL_ASSOC)) {
    $text[$row['term']] = $row['term_value'];
  }

  $form['disclaimer'] = array(
      '#type' => 'textarea',
      '#title' => t('Disclaimer'),
      '#rows' => 5,
      '#default_value' => $text['Disclaimer'],
      '#description' => t('The disclaimer text appended to an advisory.')
  );

  $form['provider'] = array(
      '#type' => 'textarea',
      '#title' => t('Provider'),
      '#rows' => 5,
      '#default_value' => $text['Provider'],
      '#description' => t('The provider text appended to an advisory.')
  );

  $form['watermark'] = array(
      '#type' => 'textfield',
      '#title' => t('Watermark'),
      '#size' => 28,
      '#default_value' => $text['Watermark'],
      '#maxlength' => 35,
      '#description' => t('The watermark is applied to the generated danger rose.')
  );

  $form['submit'] = array('#type' => 'submit', '#value' => t('Save configuration'));
  $form['#submit'] = array('forecast_advisory_text_settings_submit');

  return $form;
}


/**
 * Submit handler
 *
 * Saves the static text settings.
 */
function forecast_advisory_text_settings_submit($form, &$form_state) {
  $value = strip_tags($form_state['values']['disclaimer'], '<b><p><a><img><strong>');
  $value = str_replace("<p>&nbsp;</p>", '', $value);
  db_query(_sql_update_static_text(), $value, "Disclaimer");

  $value = strip_tags($form_state['values']['provider'], '<b><p><a><img><strong>');
  $value = str_replace("<p>&nbsp;</p>", '', $value);
  db_query(_sql_update_static_text(), $value, "Provider");

  $value = strip_tags($form_state['values']['watermark']);
  db_query(_sql_update_static_text(), $value, "Watermark");
}


/**
 * Returns SQL for selecting the elevation settings.
 */
function _sql_select_elevation_settings() {
  return 'SELECT region, base, mid, upper FROM {forecast_region_elevation}';
}


/**
 * Returns SQL for deleting the elevation settings.
 */
function _sql_delete_elevation_settings() {
  return 'DELETE FROM {forecast_region_elevation} WHERE region = "%s"';
}


/**
 * Returns SQL for inserting the elevation settings.
 */
function _sql_insert_elevation_settings() {
  return 'INSERT INTO {forecast_region_elevation} (region, base, mid, upper)
          VALUES ("%s","%s","%s","%s")';
}


/**
 * Returns SQL for selecting the static text fields.
 */
function _sql_select_static_text() {
  return 'SELECT t.term,t.term_value FROM {forecast_component} AS c
          LEFT JOIN {forecast_term} AS t ON (c.cid = t.fk_component_id)
          WHERE c.component="Static text"';
}


/**
 * Returns SQL for updating the static text fields.
 */
function _sql_update_static_text() {
  return 'UPDATE {forecast_term} SET term_value = "%s" WHERE term = "%s"';
}

