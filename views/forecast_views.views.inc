<?php

/**
* @file
*
* Views hooks implemented for the forecast module.
*/

function forecast_views_views_data() {
  $data['forecast_advisory']['table']['group'] = t('Advisory');
  $data['forecast_advisory']['table']['base'] = array(
      'field' => 'rid',
      'title' => t('Advisories'),
      'help' => t('Published advisories using the forecast module.'),
  );
}
