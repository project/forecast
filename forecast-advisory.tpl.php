<?php
  // Template to display an advisory.
?>

<div class="forecast_advisory">
  Forecaster: <b><?php print $node_data['forecaster']; ?></b>
  <br />
  Region: <b><?php print $node_data['region']; ?></b>

  <?php $s = $node_data['extendedforecast_rating_0']; ?>
  <?php if ($s): ?>
    <p>
    <h2>Danger Rating Forecast</h2>
    <?php
      $date = date('n/j', $node_data['created']);
      $date_plus = date('n/j', strtotime('+1 day', $node_data['created']));
    ?>
    <?php print $date; ?>: <b><?php print $s; ?></b><br />
    <?php $s = $node_data['extendedforecast_rating_1']; ?>
    <?php if ($s): ?>
      <?php print $date_plus; ?>: <b><?php print $s; ?></b>
    <?php endif; ?>

    <?php $s = $node_data['extendedforecast_assessment']; ?>
    <?php if ($s): ?>
      <p><?php print $s; ?></p>
    <?php endif; ?>
    </p>
  <?php endif; ?>

  <?php $s = $node_data['alert_warning']; ?>
  <?php if ($s): ?>
    <p>
    <h2>Avalanche Warning</h2>
    <?php print $s; ?>
    </p>
    <br />
  <?php endif; ?>

  <?php $s = $node_data['alert_watch']; ?>
  <?php if ($s): ?>
    <p>
    <h2>Avalanche Watch</h2>
    <?php print $s; ?>
    </p>
    <br />
  <?php endif; ?>

  <?php $s = $node_data['announcement_special']; ?>
  <?php if ($s): ?>
    <p>
    <h2>Special Announcement</h2>
    <?php print $s; ?>
    </p>
    <br />
  <?php endif; ?>

  <?php $s = $node_data['summary_bottomline']; ?>
  <?php $img = $node_data['summary_bottomline_rose']; ?>
  <?php if ($img || $s): ?>
    <p>
    <h2>Bottom Line</h2>
    <?php
      if ($img) {
        $path = '/'. file_directory_path() .'/forecast/'. $img;
        print '<img src="'. $path .'">';
      }
    ?>
    <br />
    <?php print $s; ?>
    </p>
    <br />
  <?php endif; ?>

  <?php $s = $node_data['conditions_current']; ?>
  <?php if ($s): ?>
    <p>
    <h2>Current Conditions</h2>
    <?php print $s; ?>
    </p>
    <br />
  <?php endif; ?>

  <?php $s = $node_data['activity_recent']; ?>
  <?php if ($s): ?>
    <p>
    <h2>Recent Activity</h2>
    <?php print $s; ?>
    </p>
    <br />
  <?php endif; ?>

  <?php for ($i = 1; $i < 4; $i++):
    $t = "threat_assessment_$i";
    $s = $node_data[$t];
    if ($s): ?>
      <p>
      <h2>Threat #<?php print $i; ?></h2>
      <div>
        <table cellspacing="0" cellpadding="10">
        <tr>
          <td width="10">Threat:</td>
          <td><b>
          <?php
            $t = "threat_problem_$i";
            $s = $node_data[$t];
            print $s;
          ?>
          </b>
          </td>
        </tr>
        <tr>
          <td>Where:</td>
          <td>
          <?php
             $t = "threat_rose_$i";
             $img = $node_data[$t];
             if ($s) {
               $path = '/'. file_directory_path() .'/forecast/'. $img;
               print '<img src="'. $path .'">';
             } else {
               print '<span><i>No danger rose available.</i></span>';
             } ?>
          </td>
        </tr>
        <tr>
          <td>Probability:</td>
          <td><b>
          <?php
            $t = "threat_probability_$i";
            $s = $node_data[$t];
            switch ($s) {
              case "None":
                print "None";
                break;
              case "Low":
                print "Very Unlikely";
                break;
              case "Moderate":
                print "Possible";
                break;
              case "Considerable":
                print "Even Chance";
                break;
              case "High":
                print "Likely";
                break;
              case "Certain":
                print "Almost Certain";
                break;
              default:
                print "UNKNOWN";
                break;
            } ?>
          </b></td>
        </tr>
        <tr>
          <td>Size/Class:</td>
          <td>
          <?php
            $output = '';
            $t = "threat_size_$i";
            $s = $node_data[$t];
            switch ($s) {
              case "Class 1":
                $output .= '<b>1</b> ';
                $output .= '<span class="smallfont">Relatively harmless.</span>';
                break;
              case "Class 2":
                $output .= '<b>2</b> ';
                $output .= '<span class="smallfont">Could bury, injure or kill a person.</span>';
                break;
              case "Class 3":
                $output .= '<b>3</b> ';
                $output .= '<span class="smallfont">Could bury and destroy a car.</span>';
                break;
              case "Class 4":
                $output .= '<b>4</b> ';
                $output .= '<span class="smallfont">Could destroy a substantial amount of forest.</span>';
                break;
              case "Class 5":
                $output .= '<b>5</b> ';
                $output .= '<span class="smallfont">Could gouge the landscape.</span>';
                break;
            }
            print $output; ?>
          </td>
        </tr>
        <tr>
          <td>Trend:</td>
          <td>
          <b>
          <?php
            $t = "threat_trend_$i";
            $s = $node_data[$t];
            if ($s) { print $s; } ?>
          </b>
          <span>over the next
          <?php 
            $t = "threat_trend_duration_$i";
            $s = $node_data[$t];
            if ($s) { print $s; }
            else { print '24'; }
            print 'hrs.'; ?>
          </span>
          </td>
        </tr>
        </table>
      </div>
      <br />
      <?php
        $t = "threat_assessment_$i";
        $s = $node_data[$t];
        if ($s) { print $s; } ?>
      <br />
      </p>
      <br />
    <?php endif; ?>
  <?php endfor; ?>

  <?php $s = $node_data['weather_mountain']; ?>
  <?php if ($s): ?>
    <p>
    <h2>Mountain Weather</h2>
    <?php print $s; ?>
    </p>
    <br />
  <?php endif; ?>

  <?php $s = $node_data['announcement_general']; ?>
  <?php if ($s): ?>
    <p>
    <h2>General Announcements</h2>
    <?php print $s; ?>
    </p>
    <br />
  <?php endif; ?>

  <?php $s = $node_data['statictext_disclaimer']; ?>
  <?php if ($s): ?>
    <p>
    <?php print $s; ?>
    </p>
    <br />
  <?php endif; ?>

  <?php $s = $node_data['statictext_provider']; ?>
  <?php if ($s): ?>
    <p>
    <?php print $s; ?>
    </p>
    <br />
  <?php endif; ?>
</div>
