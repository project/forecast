Overview
========
The forecast module allows avalanche forecasters to create an advisory.
To do that, it uses Taxonomy and Profile.

Taxonomy: A list of regions for content type "Advisory."
Profile: First/last name to a forecaster and specify which region they belong.

The components that make up an advisory (Alerts, Threats, Bottom line, etc.)
can be enabled/disabled through the "Forecast configuration" links.  This
allows forecast centers to customize the look/feel/content of their
advisories.  Additional theming can be done through the node-advisory.tpl.php.

The danger rose (if that component is enabled) is an SVG document that
gets loaded through a seperate file (inc/rose.php).  To use it, simply
select the color/danger rating and the aspect/elevation.  To select
all aspects, hold ALT/CMD and click the elevation where that danger exists.
To select all elevations, click the aspect label where that danger exists.

The generated danger rose can be customized with elevation levels and
a watermark.

The clone module works great to clone a previous advisory.  Perfect for
those high pressure days when the snow stability changes little.


Installing/configuring
======================
The forecast module creates the forecaster role and an initial region for
the profile.  Use the format forecast_region_name where name is the name
of the region.  Duplicate these names in the taxonomy "Region."  This allows
you to control which regions a forecaster belongs.  For each forecaster, set
their first/last name (if desired) and the region(s).

1. Go to admin/build/modules to enable the module.
2. Go to admin/content/taxonomy to add regions.
3. Go to admin/user/profile to edit profile regions.


Customizing
===========
1. Enable the components specific to your advisory:
    admin/forecast/advisory/settings

2. Set the elevation levels for your regions:
    admin/forecast/advisory/elevation/settings

3. Set the static text/rose watermark:
    admin/forecast/advisory/text/settings

4. Set the mobile alerts conditions:
    admin/forecast/alerts/settings

5. Set the mobile alerts text:
    admin/forecast/alerts/text/settings

6. Add the recent advisory and danger ratings blocks.
    admin/build/block

7. Theme the advisory by copying and modifying forecast-advisory.tpl.php.


NEW FOR D6
==========
- Rewrite, refactor... code much easier to follow from D5 version.
- Fixed region storage in database mismatch.
- Moved admin into separate modules.
- Not storing empty roses in the database.
- Themable template.
- Sets publish/unpublished based on default from content type.
- Title of node is now "region - date."
- Removed extra icons/maps... not relevant.
- Start of views integration (needs use-cases)
- Support for Tokens module.


TODO
====
- Rose doesn't handle preview correctly (remove preview).
- Add better selenium test suite and simpletest unit tests.
- Themable form
http://api.drupal.org/api/file/modules/search/search-theme-form.tpl.php


WISHLIST
========
- Allow for number of rings in rose (fixed to 3, but could easily support 4).
- Translatable terms, use a rating value of 0-6 and allow the interface to determine
  what 3 means (Moderate/Possible/Dangerous, whatever).
- Views integration... (search on rose values?, regions, ratings, etc.)
- Change how rose values are stored so they can be easily searched?


AUTHOR
======
jason@dharmatech.org
copyright (C) 2007-2009 DharmaTech
